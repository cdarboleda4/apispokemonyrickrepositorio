console.log(" *** Get Api ***");

const url = "https://pokeapi.co/api/v2/pokemon/?offset=00&limit=10";


// obtener los datos de la api

const getData = (api, opc) => {

    return fetch(api)
        .then((response) => response.json())
        .then((json) => {
            if (opc == 0)
                obtenerUrl(json);
            else
                imprimirDatos(json);
        })
        .catch((error) => {
            console.log("Error... caray ...", error)
        })

};

let todaData;
let html;

const obtenerUrl = (data) => {

    todaData = data;
    validarPaginacion(todaData);



    data.results.forEach(pokemon => {
        html = "";
        getData(pokemon.url, 1);
    });


}

const imprimirDatos = (data) => {

    // pintar los pokemon 
    html += `<div class="cards">`;
    html += `<div>`;
    html += `<img class="formatoImg" src="${data.sprites.other.dream_world.front_default}">`;
    html += `</div>`;
    html += `<div class="contenedorTextos">`;
    html += `<small class="txtLabel"> Nombre </Small>`;
    html += `<p class="txtTexto">${data.name}</p>`;
    html += `<small class="txtLabel"> Habilidades </Small>`;

    data.abilities.forEach(habilidad => {
        html += `<p class="txtTexto">${habilidad.ability.name}</p>`;
    });


    html += `</div>`;
    html += `</div>`;


    document.getElementById("contendedorTodo").innerHTML = html

}

const validarPaginacion = (data) => {


    let atras = "";
    let adelante = "";
    let vacio = "";

    atras += `<img class="icoFlecha" src="./prev.png" alt="" >`;
    adelante += `<img class="icoFlecha" src="./next.png" alt="" >`;


    if (data.previous != null) {

        document.getElementById("atras").innerHTML = atras
    } else {
        document.getElementById("atras").innerHTML = vacio
        console.log("no existen páginas atras")
    }

    if (data.next != null) {

        document.getElementById("adelante").innerHTML = adelante
    } else {
        document.getElementById("adelante").innerHTML = vacio
        console.log("no existen páginas adelante ")
    }
}



const btnPrev = document.getElementById("btnPrev");
const btnnext = document.getElementById("btnnext");


btnPrev.addEventListener('click', () => {

    getData(todaData.previous, 0)
})

btnnext.addEventListener('click', () => {

    getData(todaData.next, 0)
})




getData(url, 0);

